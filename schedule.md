### Time Schedule

## Day 1

# Morning, Elixir Basics 9:30 (A)

- [ ] Welcome, and get everyone set up (15m)
- [ ] Foundations & Origins (20m)
- [ ] Basic types, operators, pattern matching and control flow (20m)
- [ ] Exercise 1 (20m) - Write FizzBuzz in Elixir!
- [ ] Basic tools (mix, iex, hex) (10m)
- [ ] Lists, maps, structs, binaries (20m)
- [ ] Exercise 2 (20m) - Write out the Fibonacci sequence
- [ ] Modules, alias, require, import and use (15m)
- [ ] Some other fun assignment

# Afternoon, Concurrency Basics 13:00 (T)

- [ ] Processes & inter-process communication (20m)
- [ ] Exercise 4 (30m) - A simple key-value store
- [ ] Key value store → GenServer (20m)
- [ ] Exercise 5 (30m) - GenServer implementation of key-value store
- [ ] Process supervision; applications (20m)
- [ ] Exercise 6 (60m) - Elevator GenServer live coding

- A word from our partner, Betty Blocks

## Day 2

# Morning, More wonderful Elixir (& Web) 9:30 (A)

- [ ] Plugs, endpoint, routing, controllers (20m) (Data Structure & Transformations)
- [ ] Exercise 7 (30m) - Write a hello world, using plugs
- [ ] Umbrella apps (20m)?
- [ ] Exercise 8 (45m) - Put elevator in umbrella app; add REST API
- [ ] ETS tables (20m)
- [ ] Exercise 9 (30m) - record history in elevator; add REST endpoint for elevator history

- phoenix channels elevator

# Afternoon, Doing it live!  13:00 (T)

- [ ] Deploying with distillery (20m)
- [ ] Exercise 10 (30m) - deploy locally or in VM/Docker env
- [ ] Distributed Elixir; connecting to running node, tracing and monitoring
- [ ] Exercise 11 (20m) - tracing with Tap / Observer / Erlyberly (20m)
- [ ] Hot code reloading (20m)
- [ ] Exercise 12 (30m) - start simple Cachex, implement hot code reloading
- [ ] Application upgrades, GenServer code_change callback (15m)
- [ ] Exercise 13 (20m) - change something on API (add /version or /uptime?), do a hot-deploy


## Ideas:
- specs & dyalizer
- protocols, behaviours
- macros & metaprogramming
- registry / gproc
- GenStage & Flow
- ETS, Mnesia
