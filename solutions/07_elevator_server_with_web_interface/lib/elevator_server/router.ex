defmodule ElevatorServer.Router do
  use Plug.Router

  if Mix.env == :dev do
    use Plug.Debugger
  end

  plug :match
  plug :dispatch

  post "/tick" do
    {door, current_floor, queue} = Elevator.tick(Elevator)

    conn
    |> render_json(%{door: door, current_floor: current_floor, queue: queue})
  end

  post "/goto/:floor" do
    Elevator.go_to(Elevator, String.to_integer(floor))

    conn
    |> send_resp(200, "")
  end

  get "/control" do
    info = Elevator.info(Elevator)

    conn
    |> render_control_panel(info)
  end

  get "/control_panel.css" do
    {:ok, content} = File.read("priv/control_panel.css")

    conn
    |> put_resp_content_type("text/css")
    |> send_resp(200, content)
  end

  get "/control_panel.js" do
    {:ok, content} = File.read("priv/control_panel.js")

    conn
    |> put_resp_content_type("text/javascript")
    |> send_resp(200, content)
  end

  defp render_json(conn, data) do
    json = Poison.encode!(data)

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, json)
  end

  defp render_control_panel(conn, elevator_state) do
    html = EEx.eval_file("lib/index.eex", [
      num_floors: elevator_state.num_floors,
      current_floor: elevator_state.current_floor
    ])

    conn
    |> put_resp_content_type("text/html")
    |> send_resp(200, html)
  end
end
