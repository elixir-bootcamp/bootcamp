defmodule ElevatorServer.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      Plug.Adapters.Cowboy.child_spec(:http, ElevatorServer.Router, [], port: 8080),
      worker(Elevator, [10])
    ]

    IO.puts "Started the application"

    opts = [strategy: :one_for_one, name: ElevatorServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
