defmodule Elevator do
  use GenServer
  defstruct current_floor: 1, queue: [], door: :closed, num_floors: 1

  @doc """
  Start an elevator process, in a building with `num_floors` floors.
  """
  def start_link(num_floors, start_floor \\ 1) do
    GenServer.start_link(Elevator, {num_floors, start_floor}, name: Elevator, debug: [:trace])
  end

  @doc """
  Standing on the given floor, call the elevator
  """
  def call(pid, floor) do
    GenServer.call(pid, {:call, floor})
  end

  @doc """
  Standing in the elevator, tell the elevator you want to go to the
  given floor. (e.g. pushing the floor number button).
  """
  def go_to(pid, floor) do
    GenServer.call(pid, {:call, floor})
  end

  @doc """
  Advance the elevator state
  """
  def tick(pid) do
    GenServer.call(pid, :tick)
  end

  @doc """
  Get elevator info
  """
  def info(pid) do
    GenServer.call(pid, :info)
  end

  ## Server Callbacks

  def init({num_floors, start_floor}) do
    state = %Elevator{current_floor: start_floor, num_floors: num_floors}
    {:ok, state}
  end

  def handle_call({:call, floor}, _from, state) do
    new_state = state
    |> add_floor_to_queue(floor)

    {:reply, :ok, new_state}
  end

  def handle_call(:tick, _from, state) do
    new_state = state
    |> elevator_tick

    {:reply, {new_state.door, new_state.current_floor, new_state.queue}, new_state}
  end

  def handle_call(:info, _from , state) do
    {:reply, state, state}
  end

  ## Handler functions

  defp elevator_tick(state) do
    case state do
      %{door: :open} -> close_door(state)
      %{door: :closed} -> stop_or_move(state)
    end
  end

  ## Elevator functions

  defp add_floor_to_queue(state, floor) do
    cond do
      Enum.member?(state.queue, floor) ->
        state
      true ->
        state
        |> Map.put(:queue, state.queue ++ [floor])
    end
  end

  defp close_door(state) do
    state
    |> Map.put(:door, :closed)
  end

  defp stop_or_move(state) do
    cond do
      should_stop?(state) ->
        state
        |> open_door
        |> remove_floor_from_queue
      should_move?(state) ->
        state
        |> move_to_destination
      true ->
        state
    end
  end

  defp should_stop?(state) do
    %{queue: queue, current_floor: current_floor} = state
    Enum.member?(queue, current_floor)
  end

  defp open_door(state) do
    state
    |> Map.put(:door, :open)
  end

  defp remove_floor_from_queue(state) do
    new_queue = List.delete(state.queue, state.current_floor)

    state
    |> Map.put(:queue, new_queue)
  end

  defp should_move?(state) do
    !Enum.empty?(state.queue)
  end

  defp move_to_destination(state) do
    %{current_floor: current_floor, queue: [ next_floor | _queue ]} = state

    new_current_floor = cond do
      current_floor < next_floor -> current_floor + 1
      current_floor > next_floor -> current_floor - 1
    end

    state
    |> Map.put(:current_floor, new_current_floor)
  end
end
