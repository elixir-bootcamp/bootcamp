defmodule ElevatorServer.MixProject do
  use Mix.Project

  def project do
    [
      app: :elevator_server,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {ElevatorServer.Application, []}
    ]
  end

  defp deps do
    [{:cowboy, "~> 1.1.2"},
     {:plug, "~> 1.3.4"},
     {:poison, "~> 3.1"}]
  end
end
