const numFloors = parseInt(document.body.dataset.numFloors, 10);
const elevator = document.querySelector('[data-id="elevator"]');
const buttons = Array.from(document.querySelectorAll('[data-action="call-elevator"]'));

const calculateElevatorPosition = (currentFloor) => {
  const y = (100.0 / numFloors) * (numFloors - currentFloor);
  return `${y}%`;
};

const updateActiveButtons = ({ currentFloor, queue }) => {
  buttons.forEach((button) => {
    const floorNumber = parseInt(button.dataset.floorNumber, 10);
    const buttonIsActive = queue.indexOf(floorNumber) !== -1;
    button.classList.toggle('is-active', buttonIsActive);
  });
};

const tick = () => {
  fetch('/tick', { method: 'POST' })
    .then(response => response.json())
    .then(({ door, current_floor: currentFloor, queue }) => {
      elevator.classList.toggle('is-open', door === 'open');
      elevator.style.top = calculateElevatorPosition(currentFloor);
      updateActiveButtons({ currentFloor, queue });
    });
};

const onCallElevatorButton = ({ target: button }) => {
  const floorNumber = button.dataset.floorNumber;
  button.classList.add('is-active');
  fetch(`/goto/${floorNumber}`, { method: 'POST' });
};

const timer = setInterval(tick, 1000);
buttons.forEach(button => button.addEventListener('click', onCallElevatorButton));
