defmodule HelloServer.Router do
  use Plug.Router
  plug :match
  plug :dispatch


  get "/", do: send_resp(conn, 200, "Hello, world!")

  get "/hello/:name" do
    send_resp(conn, 200, "Hello, #{name}!")
  end

  get "/json" do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(%{message: "Hello, world!"}))
  end

  match _, do: send_resp(conn, 404, "Not found.")
end
