defmodule HelloServerTest do
  use ExUnit.Case
  use Plug.Test

  alias HelloServer.Router

  describe "router" do

    test "request for / returns expected result" do
      conn = conn(:get, "/")

      # Invoke the router
      conn = Router.call(conn, [])

      assert conn.state == :sent
      assert conn.status == 200
      assert conn.resp_body == "Hello, world!"
    end

    test "any other request returns 404" do
      conn = conn(:get, "/some-non-existing/page")

      # Invoke the router
      conn = Router.call(conn, [])

      assert conn.state == :sent
      assert conn.status == 404
      assert conn.resp_body == "Not found."

    end
  end
end
