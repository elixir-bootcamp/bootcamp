defmodule ElevatorServer.Mixfile do
  use Mix.Project

  def project do
    [app: :elevator_server,
     version: "0.1.0",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [extra_applications: [:logger], mod: {ElevatorServer, []}]
  end

  defp deps do
    [{:cowboy, "~> 1.1.2"},
     {:plug, "~> 1.3.4"},
     {:poison, "~> 3.0"}]
  end
end
