defmodule ElevatorServer.Router do
  use Plug.Router

  plug :match
  plug :dispatch

  get "/" do
    {doors, current_floor} = Elevator.status(Elevator)

    conn
    |> put_resp_header("content-type", "application/json")
    |> send_resp(200, Poison.encode!(%{doors: doors, current_floor: current_floor}))
  end

  post "/tick" do
    {doors, current_floor} = Elevator.tick(Elevator)

    conn
    |> put_resp_header("content-type", "application/json")
    |> send_resp(200, Poison.encode!(%{doors: doors, current_floor: current_floor}))
  end

  post "/goto/:floor" do
    :ok = Elevator.go_to(Elevator, String.to_integer(floor))

    conn
    |> put_resp_header("content-type", "application/json")
    |> send_resp(204, "")
  end

  match _, do: send_resp(conn, 404, "Not found.")
end
