defmodule Math do
  @doc """
  Calculate fibonacci (which is the sum of previous 2 fibonaccis)

  fib(0) -> 1
  fib(1) -> 1
  fib(n) -> fib(n-2) + fib(n-1)
  0 1 2 3 4 5  6  7
  1 1 2 3 5 8 11 19
  """
  @spec fibonacci(integer) :: integer
  def fibonacci(n) do
    "Fixme"
  end

  @doc """
  sum/1 takes a list of numbers, runs it through a recursive function
  """
  def sum(numbers) do
    "Fixme"
  end
end
