defmodule FizzBuzz do
  import Kernel, except: [if: 2, unless: 2]

  @doc """
  FizzBuzz translates nummbers(from 1 to 100) into
  "fizz" if it's a multiple of 3
  "buzz" if it's a multiple of 5
  "fizzbuzz" if it's a multiple of 3 and 5
  the number itself if it doesn't match the above

  ## Examples

    iex>FizzBuzz.translate(1)
    1
    iex>FizzBuzz.translate(5)
    "fizz"
    iex>FizzBuzz.translate(3)
    "buzz"
    iex>FizzBuzz.translate(45)
    "fizzbuzz"

  """

  @spec translate(number) :: String.t() | number
  def translate(number) do
    "FIXME"
  end
end
