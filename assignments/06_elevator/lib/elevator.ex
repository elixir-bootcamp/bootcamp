defmodule Elevator do
  use GenServer

  defmodule State do
    defstruct num_floors: 1, start_floor: 1, current_floor: 1, destinations: [], door: :closed
  end

  @doc """
  Start an elevator process, in a building with `num_floors` floors.
  """
  def start(num_floors, start_floor \\ 1) do
    GenServer.start_link(Elevator, {num_floors, start_floor}, [])
  end

  @doc """
  Standing on the given floor, call the elevator
  """
  def call(pid, floor) do
    GenServer.call(pid, {:call, floor})
  end

  @doc """
  Standing in the elevator, tell the elevator you want to go to the
  given floor. (e.g. pushing the floor number button).
  """
  def go_to(pid, floor) do
    GenServer.call(pid, {:call, floor})
  end

  @doc """
  Advance the elevator state
  """
  def tick(pid) do
    GenServer.call(pid, :tick)
  end

  ## Server Callbacks

  def init({num_floors, start_floor}) do
    state = %State{num_floors: num_floors, start_floor: start_floor}
    {:ok, state}
  end

  defp update(%State{door: :open} = state) do
    Map.put(state, :door, :closed)
  end

  defp update(%State{destinations: [current_floor | others], current_floor: current_floor} = state) do
    %{state | door: :open, destinations: others}
  end

  defp update(%State{destinations: [destination | _], current_floor: current_floor} = state) do
    if current_floor > destination do
      Map.put(state, :current_floor, current_floor - 1)
    else
      Map.put(state, :current_floor, current_floor + 1)
    end
  end

  defp update(state), do: state

  def handle_call(:tick, _from, state) do
    new_state = update(state)
    {:reply, {new_state.door, new_state.current_floor}, new_state}
  end

  def handle_call({:call, floor}, _from, %{current_floor: current_floor, destinations: destinations} = state) do
    new_destinations = [floor | destinations] |> Enum.sort(sort(current_floor))
    new_state = Map.put(state, :destinations, new_destinations)
    {:reply, :ok, new_state}
  end

  defp sort(c) do
    fn (a, b) ->
      delta_a = abs(a - c)
      delta_b = abs(b - c)
      delta_a <= delta_b
    end
  end
end
