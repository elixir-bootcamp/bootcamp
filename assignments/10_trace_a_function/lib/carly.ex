defmodule Carly do
  def start do
   spawn(__MODULE__, :loop, [])
  end

  def loop do
    receive do
      {:call_me, sender} ->
        send(sender, :maybe)
        loop()
      :reload ->
        Carly.loop()
    end
  end
end
