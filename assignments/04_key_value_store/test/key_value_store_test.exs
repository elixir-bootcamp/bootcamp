defmodule KeyValueStoreTest do
  use ExUnit.Case
  doctest KeyValueStore

  setup do
    pid = KeyValueStore.start()
    {:ok, pid: pid}
  end

  test "store and retrieve value", %{pid: pid} do
    assert nil == KeyValueStore.get(pid, "name")
    assert :ok == KeyValueStore.put(pid, "name", "João")
    assert "João" == KeyValueStore.get(pid, "name")
  end

  test "store and delete value", %{pid: pid} do
    assert nil == KeyValueStore.get(pid, "name")
    assert :ok == KeyValueStore.put(pid, "name", "Joost")
    assert "Joost" == KeyValueStore.get(pid, "name")
    assert :ok == KeyValueStore.delete(pid, "name")
    assert nil == KeyValueStore.get(pid, "name")
  end

  test "store and wipe all", %{pid: pid} do
    assert nil == KeyValueStore.get(pid, "name")
    assert :ok == KeyValueStore.put(pid, "name", "Luc")
    assert "Luc" == KeyValueStore.get(pid, "name")
    assert :ok == KeyValueStore.wipe(pid)
    assert nil == KeyValueStore.get(pid, "name")
  end
end
